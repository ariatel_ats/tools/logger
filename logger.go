package logger

import (
	"gitlab.com/ariatel_ats/tools/logger/custom_log"
	"os"
	"io"
	"strings"
	"time"
)

type Logger struct {
	Path 	string
	Error   *log.Logger
    Warning *log.Logger
	Debug 	*log.Logger
    Info    *log.Logger
}


func (l *Logger) Start(days uint8) {

	directory := "/var/log"
	customDirectories := strings.Split(l.Path, "/")
	
	for i := 0 ; i < len(customDirectories) - 1 ; i++ {
		directory += "/" + customDirectories[i]
	}

	if _, err := os.Stat(directory); os.IsNotExist(err) {
		err := os.MkdirAll(directory, 0777)
		if err != nil {
			panic(err)
		}
	}
	
	totalPath := "/var/log/" + l.Path

    file, err := os.OpenFile(totalPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
    if err != nil {
        log.Fatal(err)
    }

	go func() {
		for range time.Tick(time.Second * 86400 * time.Duration(days)) {
			err = file.Truncate(0)
			if err != nil {
				log.Fatal(err)
			}
		}
	}()

	mw := io.MultiWriter(os.Stdout, file)

    l.Warning = log.New(mw, "[ WARNING ] ", log.Ldate|log.Ltime|log.Lshortfile)
    l.Error = log.New(mw, "[ ERROR ] ", log.Ldate|log.Ltime|log.Lshortfile)
	l.Info = log.New(mw, "[ INFO ] ", log.Ldate|log.Ltime|log.Lshortfile)
	l.Debug = log.New(mw, "[ DEBUG ] ", log.Ldate|log.Ltime|log.Lshortfile)
}